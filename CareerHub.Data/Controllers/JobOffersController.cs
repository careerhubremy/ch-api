﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CareerHub.DAL.Data;
using CareerHub.DAL.Models;
using CareerHub.Data.Services.ModPropsService;
using CareerHub.Data.Extensions;

namespace CareerHub.Data.Controllers
{
    [Route("api/jobOffers")]
    [ApiController]
    public class JobOffersController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IModPropsService _modPropsService;

        public JobOffersController(DataContext context, IModPropsService modPropsService)
        {
            _context = context;
            _modPropsService = modPropsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetJobOffers()
        {
            var jobOffers = await _context.JobOffers
                .Include(x => x.SubscribedStudents)
                .Include(x => x.Tags)
                .Include(x => x.Company)
                .Include(x => x.AppliedCVs)
                .Where(x => x.EndDate.Date >= DateTime.Today).ToListAsync();

            return Ok(jobOffers.Select(x => new
            {
                x.JobOfferId,
                x.CompanyId,
                x.Overview,
                x.Requirements,
                x.Responsibilities,
                x.Title,
                x.Company.CompanyName,
                x.StartDate,
                x.EndDate,
                x.Image,
                x.Company.CompanyLogo,
                x.Tags,
                TotalSubscribers = x.SubscribedStudents.Count,
                isFollowed = x.SubscribedStudents.Any(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault()),
                TotalCVs = x.AppliedCVs.Count
            }));
        }

        [HttpGet("chunk/{chunk}")]
        public async Task<IActionResult> GetJobOffers(int chunk)
        {
            var jobOffers = await _context.JobOffers
                .Include(x => x.SubscribedStudents)
                .Include(x => x.Tags)
                .Include(x => x.Company)
                .Include(x => x.AppliedCVs)
                .Where(x => x.EndDate >= DateTime.Today)
                .Chunk(chunk, 20)
                .ToListAsync();

            return Ok(jobOffers.Select(x => new
            {
                x.JobOfferId,
                x.CompanyId,
                x.Overview,
                x.Requirements,
                x.Responsibilities,
                x.Title,
                x.Company.CompanyName,
                x.StartDate,
                x.EndDate,
                x.Image,
                x.Company.CompanyLogo,
                x.Tags,
                TotalSubscribers = x.SubscribedStudents.Count,
                isFollowed = x.SubscribedStudents.Any(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault()),
                TotalCVs = x.AppliedCVs.Count
            }));
        }

        [HttpGet("ByCompany/{companyId}")]
        public async Task<IActionResult> GetJobOffersByCompany(Guid companyId)
        {
            var jobOffers = await _context.JobOffers
                .Include(x => x.SubscribedStudents)
                .Include(x => x.Tags)
                .Include(x => x.Company)
                .Include(x => x.AppliedCVs)
                .Where(x => x.CompanyId == companyId && x.EndDate.Date >= DateTime.Today).ToListAsync();

            return Ok(jobOffers.Select(x => new
            {
                x.JobOfferId,
                x.CompanyId,
                x.Overview,
                x.Requirements,
                x.Responsibilities,
                x.Title,
                x.Company.CompanyName,
                x.StartDate,
                x.EndDate,
                x.Image,
                x.Company.CompanyLogo,
                x.Tags,
                TotalSubscribers = x.SubscribedStudents.Count,
                isFollowed = x.SubscribedStudents.Any(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault()),
                TotalCVs = x.AppliedCVs.Count
            }));
        }

        [HttpGet("ByCompany/{companyId}/{chunk}")]
        public async Task<IActionResult> GetJobOffersByCompany(Guid companyId, int chunk)
        {
            var jobOffers = await _context.JobOffers
                .Include(x => x.SubscribedStudents)
                .Include(j => j.Tags)
                .Include(x => x.Company)
                .Include(x => x.AppliedCVs)
                .Where(x => x.CompanyId == companyId && x.EndDate.Date >= DateTime.Today)
                .Chunk(chunk, 20)
                .ToListAsync();

            return Ok(jobOffers.Select(x => new {
                x.JobOfferId,
                x.CompanyId,
                x.Overview,
                x.Requirements,
                x.Responsibilities,
                x.Title,
                x.Company.CompanyName,
                x.StartDate,
                x.EndDate,
                x.Image,
                x.Company.CompanyLogo,
                x.Tags,
                TotalSubscribers = x.SubscribedStudents.Count,
                isFollowed = x.SubscribedStudents.Any(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault()),
                TotalCVs = x.AppliedCVs.Count
            }));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetJobOffer(Guid id)
        {
            var role = HttpContext.Request.Headers["Role"].FirstOrDefault();

            if (role == "admin" || role == "student")
            {
                var jobOffer = await _context.JobOffers
                    .Include(x => x.Company)
                    .Include(x => x.Tags)
                    .Include(x => x.SubscribedStudents)
                    .Include(x => x.AppliedCVs)
                    .SingleOrDefaultAsync(j => j.JobOfferId == id);

                if (jobOffer == null)
                {
                    return NotFound();
                }

                return Ok(new
                {
                    jobOffer.JobOfferId,
                    jobOffer.CompanyId,
                    jobOffer.Overview,
                    jobOffer.Requirements,
                    jobOffer.Responsibilities,
                    jobOffer.Title,
                    jobOffer.Company.CompanyName,
                    jobOffer.StartDate,
                    jobOffer.EndDate,
                    jobOffer.Image,
                    jobOffer.Company.CompanyLogo,
                    jobOffer.Tags,
                    TotalSubscribers = jobOffer.SubscribedStudents.Count,
                    isFollowed = jobOffer.SubscribedStudents.Any(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault()),
                    TotalCVs = jobOffer.AppliedCVs.Count
                }); ;
            }
            else if (role == "company")
            {
                var jobOffer = await _context.JobOffers
                    .Include(j => j.AppliedCVs)
                    .SingleOrDefaultAsync(j => j.JobOfferId == id);

                if (jobOffer == null)
                {
                    return NotFound();
                }

                return Ok(jobOffer.AppliedCVs);
            }

            return BadRequest();
        }

        [HttpPost]
        public async Task<ActionResult> PostJobOffer(JobOffer jobOffer)
        {
            if (jobOffer.StartDate.Date.AddDays(60) <= jobOffer.EndDate.Date)
            {
                return BadRequest("Invalid date");
            }

            var userId = HttpContext.Request.Headers["UserId"].FirstOrDefault();

            var company = await _context.Companies.SingleOrDefaultAsync(x => x.UserId.ToString() == userId);

            company.JobOffers.Add(jobOffer);

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> PutJobOffer(JobOffer model)
        {
            if (DateTime.Today.AddDays(60) <= model.EndDate.Date)
            {
                return BadRequest("Invalid date");
            }

            var userId = HttpContext.Request.Headers["UserId"].FirstOrDefault();

            var jobOffer = await _context.JobOffers
                .Include(x => x.Company)
                .SingleOrDefaultAsync(x => x.JobOfferId == model.JobOfferId && x.Company.UserId.ToString() == userId);

            if (jobOffer == null)
            {
                return NotFound();
            }

            jobOffer.Title = model.Title;
            jobOffer.Overview = model.Overview;
            jobOffer.Requirements = model.Requirements;
            jobOffer.Responsibilities = model.Responsibilities;
            jobOffer.Image = model.Image;
            jobOffer.EndDate = model.EndDate;
            jobOffer.Tags = model.Tags;
            jobOffer.StartDate = model.StartDate;

            await _context.SaveChangesAsync();

            return Ok();
        }

    }
}
