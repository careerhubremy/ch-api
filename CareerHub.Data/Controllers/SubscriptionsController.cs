﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CareerHub.DAL.Data;
using CareerHub.DAL.Models;
using CareerHub.Data.Extensions;

namespace CareerHub.Data.Controllers
{
    [Route("api/subscriptions")]
    [ApiController]
    public class SubscriptionsController : ControllerBase
    {
        private readonly DataContext _context;

        public SubscriptionsController(DataContext context)
        {
            _context = context;
        }

        [HttpGet("companies")]
        public async Task<IActionResult> GetSubscriptionsCompanies()
        {
            var student = await _context.Students
                .Include(x => x.CompanySubscriptions).ThenInclude(x => x.SubscribedStudents)
                .Include(x => x.CompanySubscriptions).ThenInclude(x => x.JobOffers.Where(x => x.EndDate >= DateTime.Today))
                .SingleOrDefaultAsync(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault());

            if (student == null)
            {
                return NotFound("No student found for corredsponding StudentId");
            }

            return Ok(student.CompanySubscriptions.Select(x => new
            {
                x.CompanyId,
                x.CompanyName,
                x.CompanyLogo,
                x.CompanyBanner,
                x.CompanyMotto,
                x.CompanyDescription,
                TotalSubscribers = x.SubscribedStudents.Count,
                TotalJobOffers = x.JobOffers.Count
            }));
        }

        [HttpGet("companies/{chunk}")]
        public async Task<IActionResult> GetSubscriptionsCompanies(int chunk)
        {
            var student = await _context.Students
                .Include(x => x.CompanySubscriptions).ThenInclude(x => x.SubscribedStudents)
                .Include(x => x.CompanySubscriptions).ThenInclude(x => x.JobOffers.Where(x => x.EndDate >= DateTime.Today))
                .SingleOrDefaultAsync(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault());

            if (student == null)
            {
                return NotFound("No student found for corredsponding UserId");
            }

            return Ok(student.CompanySubscriptions
                .Chunk(chunk, 20)
                .Select(x => new
                {
                    x.CompanyId,
                    x.CompanyName,
                    x.CompanyLogo,
                    x.CompanyBanner,
                    x.CompanyMotto,
                    x.CompanyDescription,
                    TotalSubscribers = x.SubscribedStudents.Count,
                    TotalJobOffers = x.JobOffers.Count
                }));
        }

        [HttpGet("companiesOfStudent/{studentId}")]
        public async Task<IActionResult> GetSubscriptionsCompaniesOfStudent(Guid studentId)
        {
            var student = await _context.Students
                .Include(x => x.CompanySubscriptions).ThenInclude(x => x.SubscribedStudents)
                .Include(x => x.CompanySubscriptions).ThenInclude(x => x.JobOffers)
                .SingleOrDefaultAsync(x => x.StudentId == studentId);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(student.CompanySubscriptions.Select(x => new
            {
                x.CompanyId,
                x.CompanyName,
                x.CompanyLogo,
                x.CompanyBanner,
                x.CompanyMotto,
                x.CompanyDescription,
                TotalSubscribers = x.SubscribedStudents.Count,
                TotalJobOffers = x.JobOffers.Count
            }));
        }

        [HttpGet("companiesOfStudent/{studentId}/{chunk}")]
        public async Task<IActionResult> GetSubscriptionsCompaniesOfStudent(Guid studentId, int chunk)
        {
            var student = await _context.Students
                .Include(x => x.CompanySubscriptions).ThenInclude(x => x.SubscribedStudents)
                .Include(x => x.CompanySubscriptions).ThenInclude(x => x.JobOffers.Where(x => x.EndDate >= DateTime.Today))
                .SingleOrDefaultAsync(x => x.StudentId == studentId);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(student.CompanySubscriptions
                .Chunk(chunk, 20)
                .Select(x => new
                {
                    x.CompanyId,
                    x.CompanyName,
                    x.CompanyLogo,
                    x.CompanyBanner,
                    x.CompanyMotto,
                    x.CompanyDescription,
                    TotalSubscribers = x.SubscribedStudents.Count,
                    TotalJobOffers = x.JobOffers.Count
                }));
        }

        [HttpGet("jobOffers")]
        public async Task<IActionResult> GetSubscriptionsJobOffers()
        {
            var student = await _context.Students
                .Include(x => x.JobOfferSubscriptions).ThenInclude(x => x.SubscribedStudents)
                .Include(x => x.JobOfferSubscriptions).ThenInclude(x => x.Company)
                .SingleOrDefaultAsync(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault());

            return Ok(student.JobOfferSubscriptions.Select(x => new
            {
                x.JobOfferId,
                x.Title,
                x.Overview,
                x.Requirements,
                x.Responsibilities,
                x.Image,
                x.StartDate,
                x.EndDate,
                x.Company.CompanyName,
                TotalSubscribers = x.SubscribedStudents.Count
            }));
        }

        [HttpGet("jobOffers/{chunk}")]
        public async Task<IActionResult> GetSubscriptionsJobOffers(int chunk)
        {
            var student = await _context.Students
                .Include(x => x.JobOfferSubscriptions).ThenInclude(x => x.SubscribedStudents)
                .Include(x => x.JobOfferSubscriptions).ThenInclude(x => x.Company)
                .SingleOrDefaultAsync(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault());

            return Ok(student.JobOfferSubscriptions
                .Chunk(chunk, 20)
                .Select(x => new
                {
                    x.JobOfferId,
                    x.Title,
                    x.Overview,
                    x.Requirements,
                    x.Responsibilities,
                    x.Image,
                    x.StartDate,
                    x.EndDate,
                    x.Company.CompanyName,
                    TotalSubscribers = x.SubscribedStudents.Count
                }));
        }

        [HttpGet("jobOffersOfStudent/{studentId}")]
        public async Task<IActionResult> GetSubscriptionsJobOffersOfStudent(Guid studentId)
        {
            var student = await _context.Students
                .Include(x => x.JobOfferSubscriptions).ThenInclude(x => x.SubscribedStudents)
                .Include(x => x.JobOfferSubscriptions).ThenInclude(x => x.Company)
                .SingleOrDefaultAsync(x => x.StudentId == studentId);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(student.JobOfferSubscriptions.Select(x => new
            {
                x.JobOfferId,
                x.Title,
                x.Overview,
                x.Requirements,
                x.Responsibilities,
                x.Image,
                x.StartDate,
                x.EndDate,
                x.Company.CompanyName,
                TotalSubscribers = x.SubscribedStudents.Count
            }));
        }

        [HttpGet("jobOffersOfStudent/{studentId}/{chunk}")]
        public async Task<IActionResult> GetSubscriptionsJobOffersOfStudent(Guid studentId, int chunk)
        {
            var student = await _context.Students
                .Include(x => x.JobOfferSubscriptions).ThenInclude(x => x.SubscribedStudents)
                .Include(x => x.JobOfferSubscriptions).ThenInclude(x => x.Company)
                .SingleOrDefaultAsync(x => x.StudentId == studentId);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(student.JobOfferSubscriptions
                .Chunk(chunk, 20)
                .Select(x => new
                {
                    x.JobOfferId,
                    x.Title,
                    x.Overview,
                    x.Requirements,
                    x.Responsibilities,
                    x.Image,
                    x.StartDate,
                    x.EndDate,
                    x.Company.CompanyName,
                    TotalSubscribers = x.SubscribedStudents.Count
                }));
        }

        [HttpPut("subscribe/company/{companyId}")]
        public async Task<IActionResult> SubscribeToCompany(Guid companyId)
        {
            var student = await _context.Students
                .Include(x => x.CompanySubscriptions)
                .SingleOrDefaultAsync(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault());

            if (student == null || student.CompanySubscriptions.Any(x => x.CompanyId == companyId))
            {
                return NotFound();
            }

            var company = await _context.Companies
                .SingleOrDefaultAsync(x => x.CompanyId == companyId);

            if (company == null)
            {
                return NotFound();
            }

            student.CompanySubscriptions.Add(company);

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPut("unsubscribe/company/{companyId}")]
        public async Task<IActionResult> UnsubscribeFromCompany(Guid companyId)
        {
            var student = await _context.Students
                .Include(x => x.CompanySubscriptions)
                .SingleOrDefaultAsync(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault());

            if (student == null)
            {
                return NotFound();
            }

            var company = student.CompanySubscriptions.SingleOrDefault(x => x.CompanyId == companyId);

            if (company == null)
            {
                return NotFound();
            }

            student.CompanySubscriptions.Remove(company);

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPut("subscribe/jobOffer/{id}")]
        public async Task<IActionResult> SubscribeToJobOffer(Guid id)
        {
            var student = await _context.Students
                .Include(x => x.JobOfferSubscriptions)
                .SingleOrDefaultAsync(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault());

            if (student == null || student.JobOfferSubscriptions.Any(x => x.JobOfferId == id))
            {
                return NotFound();
            }

            var jobOffer = await _context.JobOffers
                .SingleOrDefaultAsync(x => x.JobOfferId == id);

            if (jobOffer == null)
            {
                return NotFound();
            }

            student.JobOfferSubscriptions.Add(jobOffer);

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPut("unsubscribe/jobOffer/{id}")]
        public async Task<IActionResult> UnsubscribeFromJobOffer(Guid id)
        {
            var student = await _context.Students
                .Include(x => x.JobOfferSubscriptions)
                .SingleOrDefaultAsync(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault());

            if (student == null)
            {
                return NotFound();
            }

            var jobOffer = student.JobOfferSubscriptions.SingleOrDefault(x => x.JobOfferId == id);

            if (jobOffer == null)
            {
                return NotFound();
            }

            student.JobOfferSubscriptions.Remove(jobOffer);

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpGet("students")]
        public async Task<ActionResult<IEnumerable<JobOffer>>> GetSubscriptionsStudents()
        {
            var foundSubsciptions = await _context.StudentSubscriptions
                .Include(x => x.SubscriptionOwner)
                .Include(x => x.SubscriptionTarget)
                .ThenInclude(x => x.User)
                .Where(x => x.SubscriptionOwner.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault())
                .Select(x => x.SubscriptionTarget).ToListAsync();

            if (foundSubsciptions == null)
            {
                return NotFound("No subscriptions found for requested user");
            }

            return Ok(foundSubsciptions.Select(x => new 
            {
                x.StudentId,
                x.LastName,
                x.FirstName,
                x.Phone,
                x.Group,
                x.Photo,
                x.UserId,
                x.User.Email,
                x.BirthDate
            }));
        }

        [HttpPut("unsubscribe/student/{studentId}")]
        public async Task<IActionResult> UnsubscribeFromStudent(Guid studentId)
        {
            var subscribedStudent = await _context.Students
                .Include(x => x.StudentSubscriptions)
                .SingleOrDefaultAsync(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault());

            if (subscribedStudent == null)
            {
                return NotFound("Student which sent request not found");
            }

            var subscriptionToCancel = subscribedStudent.StudentSubscriptions
                .Where(x => x.SubscriptionTargetId == studentId).FirstOrDefault();

            if (subscriptionToCancel == null)
            {
                return NotFound("Subscription to cancel not found");
            }
            _context.StudentSubscriptions.Remove(subscriptionToCancel);
            await _context.SaveChangesAsync();

            return Ok("Successfully unsubscribed from student");
        }

        [HttpPut("subscribe/student/{studentId}")]
        public async Task<IActionResult> SubscribeToStudent(Guid studentId)
        {
            var studentThatSentRequest = await _context.Students
                .Include(x => x.StudentSubscriptions)
                .SingleOrDefaultAsync(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault());

            if (studentThatSentRequest == null)
            {
                return NotFound("Student which sent request not found");
            }

            if (studentThatSentRequest.StudentSubscriptions.Any(x => x.SubscriptionTargetId == studentId))
            {
                return BadRequest("Subscription to student already exists");
            }

            var studentToSubscribeTo = await _context.Students
                .SingleOrDefaultAsync(x => x.StudentId == studentId);

            if (studentToSubscribeTo == null)
            {
                return NotFound("Student to subscribe to not found");
            }

            _context.StudentSubscriptions.Add(
                new StudentSubscription
                {
                    SubscriptionOwnerId = studentThatSentRequest.StudentId,
                    SubscriptionTargetId = studentToSubscribeTo.StudentId,
                }
            );
            await _context.SaveChangesAsync();

            return Ok("Successfully subscribed to student");
        }
    }
}
