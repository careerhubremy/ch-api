﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CareerHub.DAL.Data;
using CareerHub.DAL.Models;

namespace CareerHub.Data.Controllers
{
    [Route("api/experiences")]
    [ApiController]
    public class ExperiencesController : ControllerBase
    {
        private readonly DataContext _context;

        public ExperiencesController(DataContext context)
        {
            _context = context;
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetExperiences()
        {
            var userId = HttpContext.Request.Headers["UserId"].FirstOrDefault();

            var student = await _context.Students
                .Include(x => x.Experiences)
                .SingleOrDefaultAsync(x => x.UserId.ToString() == userId);

            if(student == null)
            {
                return NotFound();
            }
            
            return Ok(student.Experiences.Select(x => new { x.ExperienceId, x.StartDate, x.EndDate, x.CompanyName, x.Title, x.StudentId }));
        }

        [HttpGet("allOfStudent")]
        public async Task<IActionResult> GetExperiencesOfStudent(Guid userId)
        {
            var student = await _context.Students
                .Include(x => x.Experiences)
                .SingleOrDefaultAsync(x => x.UserId == userId);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(student.Experiences.Select(x => new { x.ExperienceId, x.StartDate, x.EndDate, x.CompanyName, x.Title, x.StudentId }));
        }

        [HttpDelete("delete/{experienceId}")]
        public async Task<IActionResult> DeleteExperience(Guid experienceId)
        {
            var userId = HttpContext.Request.Headers["UserId"].FirstOrDefault();

            var experience = await _context.Experiences
                .Include(x => x.Student)
                .SingleOrDefaultAsync(x => x.ExperienceId == experienceId && x.Student.UserId.ToString() == userId);

            if (experience == null)
            {
                return NotFound();
            }

            _context.Experiences.Remove(experience);

            _context.SaveChanges();

            return Ok();
        }

        [HttpPost("create")]
        public async Task<IActionResult> PostExperience(Experience experience)
        {
            if(experience.StartDate > experience.EndDate)
            {
                return BadRequest("Start date is greater then end date");
            }
            var userId = HttpContext.Request.Headers["UserId"].FirstOrDefault();

            var student = await _context.Students
                .SingleOrDefaultAsync(x => x.UserId.ToString() == userId);

            if (student == null)
            {
                return NotFound();
            }

            student.Experiences.Add(experience);

            _context.SaveChanges();

            return Ok(new { experience.ExperienceId, experience.StartDate, experience.EndDate, experience.CompanyName, experience.Title, experience.StudentId });
        }

        [HttpPut("edit")]
        public async Task<IActionResult> PutExperience(Experience model)
        {
            if (model.StartDate > model.EndDate)
            {
                return BadRequest("Start date is greater then end date");
            }

            var userId = HttpContext.Request.Headers["UserId"].FirstOrDefault();

            var experience = await _context.Experiences
                .Include(x => x.Student)
                .SingleOrDefaultAsync(x => x.ExperienceId == model.ExperienceId && x.Student.UserId.ToString() == userId);

            if (experience == null)
            {
                return NotFound();
            }

            experience.Title = model.Title;
            experience.StartDate = model.StartDate;
            experience.EndDate = model.EndDate;
            experience.CompanyName = model.CompanyName;

            _context.SaveChanges();

            return Ok();
        }
    }
}
