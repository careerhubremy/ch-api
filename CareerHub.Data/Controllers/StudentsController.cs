﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CareerHub.DAL.Data;
using CareerHub.DAL.Models;
using CareerHub.DAL.ModelsDTO.Requests;
using System.Security.Cryptography;
using System.Text;
using CareerHub.Data.Extensions;

namespace CareerHub.Data.Controllers
{
    [Route("api/students")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly DataContext _context;

        public StudentsController(DataContext context)
        {
            _context = context;
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetStudents()
        {
            var students = await _context.Students
                .Include(x => x.User)
                .Where(x => x.UserId.ToString() != HttpContext.Request.Headers["UserId"].FirstOrDefault())
                .ToListAsync();
            return Ok(students.Select(x => new
            {
                x.StudentId,
                x.LastName,
                x.FirstName,
                x.Phone,
                x.Group,
                x.Photo,
                x.UserId,
                x.User.Email,
                x.BirthDate
            }));
        }

        [HttpGet("all/chunk/{chunk}")]
        public async Task<IActionResult> GetStudents(int chunk)
        {
            var students = await _context.Students
                .Include(x => x.User)
                .Where(x => x.UserId.ToString() != HttpContext.Request.Headers["UserId"].FirstOrDefault())
                .Chunk(chunk, 20)
                .ToListAsync();
            return Ok(students.Select(x => new
            {
                x.StudentId,
                x.LastName,
                x.FirstName,
                x.Phone,
                x.Group,
                x.Photo,
                x.UserId,
                x.User.Email,
                x.BirthDate
            }));
        }

        // всі
        [HttpGet("one/{userId}")]
        public async Task<IActionResult> GetStudent(Guid userId)
        {
            var student = await _context.Students.SingleOrDefaultAsync(x => x.UserId == userId);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(new
            {
                student.StudentId,
                student.LastName,
                student.FirstName,
                student.Phone,
                student.Group,
                student.Photo,
                student.UserId,
                student.User.Email,
                student.BirthDate
            });
        }

        // admin
        [HttpPost("create")]
        public async Task<IActionResult> PostStudent(CreateStudentRequest student)
        {
            if (await _context.Users.AnyAsync(x => x.Email == student.Email))
            {
                return BadRequest("User with such Email exists");
            }

            var newUser = new User
            {
                Email = student.Email,
                Password = GetPasswordHash(student.Password),
                Role = await _context.Roles.SingleOrDefaultAsync(x => x.Title == "student")
            };

            await _context.Users.AddAsync(newUser);

            var newStudent = new Student
            {
                User = newUser,
                LastName = student.LastName,
                FirstName = student.FirstName,
                Phone = student.Phone,
                Photo = student.Photo,
                Group = student.Group,
                BirthDate = student.BirthDate
            };

            await _context.Students.AddAsync(newStudent);

            await _context.SaveChangesAsync();

            return Ok(new
            {
                newStudent.StudentId,
                newStudent.LastName,
                newStudent.FirstName,
                newStudent.Phone,
                newStudent.Group,
                newStudent.Photo,
                newUser.UserId,
                newUser.Email
            });
        }

        // admin student
        [HttpDelete("delete/{userId?}")]
        public async Task<IActionResult> DeleteStudent(Guid userId)
        {
            var role = HttpContext.Request.Headers["Role"].FirstOrDefault();

            var user = await _context.Users
                .SingleOrDefaultAsync(x => x.UserId.ToString() == (role == "admin" ? userId.ToString() : HttpContext.Request.Headers["UserId"].FirstOrDefault()));

            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);

            _context.SaveChanges();

            return Ok();
        }

        [HttpPut("edit")]
        public async Task<IActionResult> UpdateStudent(Student model)
        {
            var role = HttpContext.Request.Headers["Role"].FirstOrDefault();

            var student = await _context.Students
                .SingleOrDefaultAsync(x => x.UserId.ToString() == (role == "admin" ? model.UserId.ToString() : HttpContext.Request.Headers["UserId"].FirstOrDefault()));

            if (student == null)
            {
                return NotFound();
            }

            student.LastName = model.LastName;
            student.FirstName = model.FirstName;
            student.Phone = model.Phone;
            student.Photo = model.Photo;
            student.Group = model.Group;

            _context.SaveChanges();

            return Ok();
        }

        private string GetPasswordHash(string password)
        {
            byte[] hash;
            using (var sha1 = new SHA256CryptoServiceProvider())
                hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(password));
            return Convert.ToBase64String(hash);
        }
    }
}
