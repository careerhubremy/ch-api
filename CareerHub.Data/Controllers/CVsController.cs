﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CareerHub.DAL.Data;
using CareerHub.DAL.Models;

namespace CareerHub.Data.Controllers
{
    [Route("api/cv")]
    [ApiController]
    public class CVsController : ControllerBase
    {
        private readonly DataContext _context;

        public CVsController(DataContext context)
        {
            _context = context;
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetCVs()
        {
            var userId = HttpContext.Request.Headers["UserId"].FirstOrDefault();

            var student = await _context.Students
                .Include(x => x.CVs)
                .SingleOrDefaultAsync(x => x.UserId.ToString() == userId);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(student.CVs.Select(x => new
            {
                x.CVId,
                x.StudentName,
                x.StudentSurname,
                x.TargetJobTitle,
                x.Photo,
                x.Goals,
                x.SkillsAndTechnologies,
                x.CreationDate,
                x.LastModificationDate,
                IsApplied = x.TargetJobOffers.Any(),
                x.StudentId
            }));
        }

        [HttpGet("allOfStudent")]
        public async Task<IActionResult> GetCVsOfStudent(Guid userId)
        {
            var student = await _context.Students
                .Include(x => x.CVs)
                .SingleOrDefaultAsync(x => x.UserId == userId);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(student.CVs.Select(x => new
            {
                x.CVId,
                x.StudentName,
                x.StudentSurname,
                x.TargetJobTitle,
                x.Photo,
                x.Goals,
                x.SkillsAndTechnologies,
                x.CreationDate,
                x.LastModificationDate,
                x.TargetJobOffers,
                x.StudentId
            }));
        }

        [HttpDelete("delete/{CVId}")]
        public async Task<IActionResult> DeleteCV(Guid CVId)
        {
            var userId = HttpContext.Request.Headers["UserId"].FirstOrDefault();

            var cv = await _context.CVs
                .Include(x => x.Student)
                .SingleOrDefaultAsync(x => x.CVId == CVId && x.Student.UserId.ToString() == userId);

            if (cv == null)
            {
                return NotFound();
            }

            _context.CVs.Remove(cv);

            _context.SaveChanges();

            return Ok();
        }

        [HttpPost("create")]
        public async Task<IActionResult> PostCV(CV cv)
        {
            var userId = HttpContext.Request.Headers["UserId"].FirstOrDefault();

            var student = await _context.Students
                .SingleOrDefaultAsync(x => x.UserId.ToString() == userId);

            if (student == null)
            {
                return NotFound();
            }

            student.CVs.Add(cv);

            _context.SaveChanges();

            return Ok(student.CVs.Select(x => new
            {
                x.CVId,
                x.StudentName,
                x.StudentSurname,
                x.TargetJobTitle,
                x.Photo,
                x.Goals,
                x.SkillsAndTechnologies,
                x.CreationDate,
                x.LastModificationDate,
                x.StudentId
            }));
        }
    }
}
