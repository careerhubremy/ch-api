﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CareerHub.DAL.Data;
using CareerHub.DAL.Models;
using CareerHub.Data.Extensions;

namespace CareerHub.Data.Controllers
{
    [Route("api/companies")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly DataContext _context;

        public CompaniesController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetCompanies()
        {
            var role = HttpContext.Request.Headers["Role"].FirstOrDefault();

            var companies = await _context.Companies
                .Include(x => x.AdditionalLinks)
                .Include(x => x.User)
                .Include(x => x.CompanyAuthLinks)
                .Include(x => x.JobOffers.Where(x => x.EndDate >= DateTime.Today))
                .Include(x => x.SubscribedStudents)
                .Where(x => role == "admin" ? true : x.CompanyAuthLinks.Count == 0)
                .ToListAsync();

            return Ok(companies.Select(x => new
            {
                x.CompanyId,
                x.CompanyName,
                x.CompanyLogo,
                x.CompanyMotto,
                x.CompanyBanner,
                x.CompanyDescription,
                x.IsFirstTime,
                x.User.Email,
                AdditionalLinks = x.AdditionalLinks.Select(y => new { y.Title, y.Uri, y.AdditionalLinkId }),
                TotalJobOffers = x.JobOffers.Count,
                TotalSubscribers = x.SubscribedStudents.Count,
                IsFollowed = x.SubscribedStudents.Any(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault())
            }));
        }

        [HttpGet("chunk/{chunk}")]
        public async Task<IActionResult> GetCompanies(int chunk)
        {
            var role = HttpContext.Request.Headers["Role"].FirstOrDefault();

            var companies = await _context.Companies
                .Include(x => x.AdditionalLinks)
                .Include(x => x.User)
                .Include(x => x.CompanyAuthLinks)
                .Include(x => x.JobOffers.Where(x => x.EndDate >= DateTime.Today))
                .Include(x => x.SubscribedStudents)
                .Where(x => role == "admin" ? true : x.CompanyAuthLinks.Count == 0)
                .Chunk(chunk, 20)
                .ToListAsync();

            return Ok(companies.Select(x => new
            {
                x.CompanyId,
                x.CompanyName,
                x.CompanyLogo,
                x.CompanyMotto,
                x.CompanyBanner,
                x.CompanyDescription,
                x.User.Email,
                AdditionalLinks = x.AdditionalLinks.Select(y => new { y.Title, y.Uri, y.AdditionalLinkId }),
                TotalSubscribers = x.SubscribedStudents.Count,
                isFollowed = x.SubscribedStudents.Any(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault())
            }));
        }

        [HttpGet("{companyId}")]
        public async Task<IActionResult> GetCompany(Guid companyId)
        {
            var company = await _context.Companies
                .Include(x => x.JobOffers.Where(x => x.EndDate >= DateTime.Today)).ThenInclude(x => x.Tags)
                .Include(x => x.AdditionalLinks)
                .Include(x => x.User)
                .Include(x => x.SubscribedStudents)
                .SingleOrDefaultAsync(x => x.CompanyId == companyId);

            if (company == null)
            {
                return NotFound();
            }

            return Ok(new
            {
                company.CompanyId,
                company.CompanyName,
                company.CompanyLogo,
                company.CompanyMotto,
                company.CompanyBanner,
                company.CompanyDescription,
                company.AdditionalLinks,
                company.JobOffers,
                company.User.Email,
                TotalSubscribers = company.SubscribedStudents.Count,
                TotalJobOffers = company.JobOffers.Count,
                isFollowed = company.SubscribedStudents.Any(x => x.UserId.ToString() == HttpContext.Request.Headers["UserId"].FirstOrDefault())
            });
        }

        [HttpDelete("delete/{companyId}")]
        public async Task<IActionResult> DeleteCompany(Guid companyId)
        {
            var company = await _context.Companies
                .SingleOrDefaultAsync(x => x.CompanyId == companyId);

            if (company == null)
            {
                return NotFound();
            }

            _context.Companies.Remove(company);

            _context.SaveChanges();

            return Ok();
        }

        [HttpPut("edit/{companyId}")]
        public /*async*/ Task<IActionResult> UpdateCompany(Guid companyId, Company model)
        {
            throw new NotImplementedException(); 
            /* var role = HttpContext.Request.Headers["Role"].FirstOrDefault();

            var company = await _context.Companies
                .SingleOrDefaultAsync(x => x.UserId.ToString() == (role == "admin" ? model.UserId.ToString() : HttpContext.Request.Headers["UserId"].FirstOrDefault()));

            if (company == null)
            {
                return NotFound();
            }

            company.CompanyName = model.CompanyName;
            company.CompanyLogo = model.CompanyLogo;
            company.CompanyBanner = model.CompanyBanner;
            company.CompanyMotto = model.CompanyMotto;
            company.CompanyDescription = model.CompanyDescription;
            company.AdditionalLinks = model.AdditionalLinks;

            _context.SaveChanges();

            return Ok(); */
        }
    }
}
