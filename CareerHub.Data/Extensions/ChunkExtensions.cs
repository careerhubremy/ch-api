﻿using System.Collections.Generic;
using System.Linq;

namespace CareerHub.Data.Extensions
{
    public static class ChunkExtensions
    {
        public static IQueryable<T> Chunk<T>(this IQueryable<T> allRecords, int chunk, int amount)
        {
            return allRecords
                .Skip(chunk <= 0 ? 0 : chunk * amount)
                .Take(amount);
        }

        public static IEnumerable<T> Chunk<T>(this IEnumerable<T> allRecords, int chunk, int amount)
        {
            return allRecords
                .Skip(chunk <= 0 ? 0 : chunk * amount)
                .Take(amount);
        }
    }
}
