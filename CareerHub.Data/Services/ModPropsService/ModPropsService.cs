﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CareerHub.Data.Services.ModPropsService.Models;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CareerHub.Data.Services.ModPropsService
{
    public class ModPropsService: IModPropsService
    {
        public void ConfigureModelEditing(EntityEntry entity, ModelEditingOptions options)
        {
            if (!options.IsEditAllList)
            {
                NotEditAllList(entity);
            }
            if (!options.IsEditAllNavEntity)
            {
                NotEditAllNavProps(entity);
            }

            foreach (var props in entity.Properties)
            {
                if (options.IgnoreList.Contains(props.Metadata.Name))
                {
                    props.IsModified = false;
                }
            }
        }

        private void NotEditAllList(EntityEntry entity)
        {
            foreach (var list in entity.Collections.Where(c=>c.GetType().Name == "List`1"))
            {
                list.IsModified = false;
            }
        }

        private void NotEditAllNavProps(EntityEntry entity)
        {
            var defaultPropTypes = new string[] { "string", "DateTime", "bool", "float", "double", "Guid", "int"};
            foreach (var property in entity.Properties.Where(p=>!defaultPropTypes.Contains(p.GetType().Name)))
            {
                property.IsModified = false;
            }
        }
    }
}
