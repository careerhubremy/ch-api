﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CareerHub.Data.Services.ModPropsService.Models
{
    public class ModelEditingOptions
    {
        public bool IsEditAllList { set; get; } = true;
        public bool IsEditAllNavEntity { set; get; } = true;
        public List<string> IgnoreList { set; get; } = new();
    }
}
