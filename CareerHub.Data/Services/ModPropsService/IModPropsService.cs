﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CareerHub.Data.Services.ModPropsService.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CareerHub.Data.Services.ModPropsService
{
    public interface IModPropsService
    {
        public void ConfigureModelEditing(EntityEntry entity, ModelEditingOptions options);
    }
}
