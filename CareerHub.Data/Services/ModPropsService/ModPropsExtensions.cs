﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace CareerHub.Data.Services.ModPropsService
{
    public static class ModPropsExtensions
    {
        public static void AddModPropsService(this IServiceCollection collection)
        {
            collection.AddTransient<IModPropsService, ModPropsService>();
        }
    }
}
