﻿using System;
using CareerHub.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace CareerHub.DAL.Data
{
    public class DataContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentSubscription> StudentSubscriptions { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<CV> CVs { get; set; }
        public DbSet<Experience> Experiences { get; set; }
        public DbSet<JobOffer> JobOffers { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RestorePasswordLog> RestorePasswordLogs { get; set; }
        public DbSet<CompanyAuthLink> CompanyAuthLinks { set; get; }
        public DbSet<StudentInfoLog> StudentInfoLogs { set; get; }
        public DbSet<AdditionalLink> AdditionalLinks { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<ForeignLanguage> ForeignLanguages { get; set; }
        public DbSet<JobType> JobTypes { get; set; }
        public DbSet<TemplateLanguage> TemplateLanguages { get; set; }


        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StudentSubscription>()
                .HasOne(s => s.SubscriptionOwner)
                .WithMany(x => x.StudentSubscriptions)
                .HasForeignKey(s => s.SubscriptionOwnerId);

            modelBuilder.Entity<StudentSubscription>()
               .HasKey(x => new { x.SubscriptionOwnerId, x.SubscriptionTargetId });

            modelBuilder.Entity<Role>().HasData(
                new Role()
                {
                    RoleId = 1,
                    Title = "student"
                },
                new Role()
                {
                    RoleId = 2,
                    Title = "company"
                },
                new Role()
                {
                    RoleId = 3,
                    Title = "admin"
                }
            );

            modelBuilder.Entity<TemplateLanguage>().HasData(
                new TemplateLanguage()
                {
                    TemplateLanguageId = 1,
                    Name = "UA"
                },
                new TemplateLanguage()
                {
                    TemplateLanguageId = 2,
                    Name = "EN"
                }
            );

            modelBuilder.Entity<JobType>().HasData(
                new JobType()
                {
                    JobTypeId = 1,
                    Name = "Software development"
                },
                new JobType()
                {
                    JobTypeId = 2,
                    Name = "Quality assurance"
                },
                new JobType()
                {
                    JobTypeId = 3,
                    Name = "Design"
                },
                new JobType()
                {
                    JobTypeId = 4,
                    Name = "DevOps"
                },
                new JobType()
                {
                    JobTypeId = 6,
                    Name = "Support"
                },
                new JobType()
                {
                    JobTypeId = 7,
                    Name = "HR"
                },
                new JobType()
                {
                    JobTypeId = 8,
                    Name = "Finances"
                },
                new JobType()
                {
                    JobTypeId = 9,
                    Name = "Other"
                }
            );
            base.OnModelCreating(modelBuilder);
        }
    }
}
