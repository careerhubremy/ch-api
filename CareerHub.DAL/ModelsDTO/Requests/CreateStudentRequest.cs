﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.ModelsDTO.Requests
{
    public class CreateStudentRequest
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public byte[] Photo { get; set; }
        public string Phone { get; set; }
        public string Group { get; set; }
        [Required]
        [RegularExpression(@"^[\w-\.]+@nure\.ua$")]
        public string Email { get; set; }
        [Required]
        [StringLength(32, MinimumLength = 8)]
        public string Password { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
