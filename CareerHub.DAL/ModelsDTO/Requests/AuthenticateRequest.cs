﻿using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.ModelsDTO.Requests
{
    public class AuthenticateRequest
    {
        [Required]
        [EmailAddress]
        public string Email { set; get; }
        [Required]
        [StringLength(32, MinimumLength = 8)]
        public string Password { set; get; }
    }
}
