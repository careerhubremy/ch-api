﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareerHub.DAL.ModelsDTO.Requests
{
    public class InternalRegisterCompanyRequest:RegisterCompanyRequest
    {
        public string FrontDomain { get; set; }
    }
}
