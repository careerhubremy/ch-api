using System;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class ForeignLanguage
    {
        public Guid ForeignLanguageId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [MaxLength(3)]
        public string Level { get; set; }
        public Guid CVId { get; set; }
        public CV CV { get; set; }
    }
}
