﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class JobOffer
    {
        [Key]
        public Guid JobOfferId { get; set; }
        [Required]
        public string Title { set; get; }
        [Required]
        public string Overview { set; get; }
        [Required]
        public string Requirements { set; get; }
        [Required]
        public string Responsibilities { set; get; }
        public byte[] Image { set; get; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        public Guid CompanyId { get; set; }
        public Company Company { get; set; }
        public int JobTypeId { get; set; }
        public JobType JobType { get; set; }
        public List<Tag> Tags { get; set; } = new();
        public List<CV> AppliedCVs { get; set; } = new();
        public List<Student> SubscribedStudents { get; set; } = new();
    }
}
