﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class Tag
    {
        public Guid TagId { get; set; }
        [Required]
        public string Title { get; set; }
        public List<JobOffer> JobOffers { get; set; } = new();
    }   
}
