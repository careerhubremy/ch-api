using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class TemplateLanguage
    {
        [Key]
        public int TemplateLanguageId { get; set; }
        [Required]
        public string Name { get; set; }
        public List<CV> CVs { get; set; } = new();
    }
}
