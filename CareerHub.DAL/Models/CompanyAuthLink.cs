﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class CompanyAuthLink
    {
        [Key]
        public Guid CompanyAuthLinkId { set; get; }
        public Guid CompanyId { set; get; }
        public Company Company { set; get; }
    }
}
