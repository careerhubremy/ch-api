﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class User
    {
        public Guid UserId { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }
        public Student Student { get; set; }
        public Company Company { get; set; }
        public List<RestorePasswordLog> RestorePasswordLogs { get; set; }
    }
}
