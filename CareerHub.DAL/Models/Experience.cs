﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class Experience
    {
        public Guid ExperienceId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string EmploymentType { get; set; }
        public string JobLocation { get; set; }
        public bool ShownInProfile { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<CV> CVs { get; set; } = new();
        public Guid StudentId { get; set; }
        public Student Student { get; set; }
    }
}
