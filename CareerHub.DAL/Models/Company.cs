﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class Company
    {
        [Key]
        public Guid CompanyId { get; set; }
        [MaxLength(40)]
        public string CompanyName { get; set; }
        public byte[] CompanyLogo { get; set; }
        public byte[] CompanyBanner { get; set; }
        [MaxLength(120)]
        public string CompanyMotto { get; set; }
        public string CompanyDescription { get; set; }
        public bool IsFirstTime { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }
        public List<Student> SubscribedStudents { get; set; } = new();
        public List<JobOffer> JobOffers { get; set; } = new();
        public List<AdditionalLink> AdditionalLinks { get; set; } = new();
        public List<CompanyAuthLink> CompanyAuthLinks { get; set; } = new();
    }
}
