﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class AdditionalLink
    {
        [Key]
        public Guid AdditionalLinkId { get; set; }
        [MaxLength(40)]
        public string Title { get; set; }
        public string Uri { get; set; }

        public Guid CompanyId { get; set; }
        public Company Company { get; set; }
    }
}
