﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareerHub.DAL.Models
{
    public  class RestorePasswordLog
    {
        public Guid RestorePasswordLogId { set; get; }
        public User User { set; get; }
        public Guid UserId { set; get; }
        public string PasswordHash { set; get; }
    }
}
