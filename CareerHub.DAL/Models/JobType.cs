using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class JobType
    {
        [Key]
        public int JobTypeId { get; set; }
        [Required]
        public string Name { get; set; }
        public List<JobOffer> JobOffers { get; set; } = new();
        public List<CV> CVs { get; set; } = new();
    }
}
