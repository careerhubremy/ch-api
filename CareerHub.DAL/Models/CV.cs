﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class CV
    {
        //TODO: create add CV request model 
        public Guid CVId { get; set; }
        public string StudentName { get; set; }
        public string StudentSurname { get; set; }
        public string TargetJobTitle { get; set; }
        public byte[] Photo { get; set; }
        public string Goals { get; set; }
        public string SkillsAndTechnologies { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastModificationDate { get; set; }
        [Required]
        public bool Finished { get; set; }

        public int JobTypeId { get; set; }
        public JobType JobType { get; set; }

        public Guid StudentId { get; set; }
        public Student Student { get; set; }

        public int TemplateLanguageId { get; set; }
        public TemplateLanguage TemplateLanguage { get; set; }

        public List<ForeignLanguage> ForeignLanguages { get; set; } = new();
        public List<Experience> Experiences { get; set; } = new();
        public List<Education> Educations { get; set; } = new();
        public List<JobOffer> TargetJobOffers { get; set; } = new();
    }
}
