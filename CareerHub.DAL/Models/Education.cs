using System;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class Education
    {
        [Key]
        public Guid EducationId { get; set; }
        [Required]
        public string University { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Degree { get; set; }
        [Required]
        public int StartYear { get; set; }
        public int? EndYear { get; set; }
        public Guid CVId { get; set; }
        public CV CV { get; set; }
    }
}
