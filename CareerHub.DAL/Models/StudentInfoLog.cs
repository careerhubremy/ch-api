﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareerHub.DAL.Models
{
    public class StudentInfoLog
    {
        public Guid StudentInfoLogId { get; set; }
        public string Email { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Group { set; get; }
    }
}
