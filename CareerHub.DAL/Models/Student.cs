﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerHub.DAL.Models
{
    public class Student
    {
        [Key]
        public Guid StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte[] Photo { get; set; }
        public string Phone { get; set; }
        public string Group { get; set; }
        public DateTime? BirthDate { get; set; }
        public List<CV> CVs { get; set; } = new();

        public Guid UserId { get; set; }
        public User User { get; set; }
        public List<Experience> Experiences { get; set; } = new();
        public List<Company> CompanySubscriptions { get; set; } = new();
        public List<JobOffer> JobOfferSubscriptions { get; set; } = new();
        public List<StudentSubscription> StudentSubscriptions { get; set; } = new();
    }
}
