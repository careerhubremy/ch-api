﻿using CareerHub.Auth.Services.Authorization.Models;

namespace CareerHub.Auth.Services.Authorization
{
    public interface IJwtService
    {
        public string GetToken(JwtUser user);
    }
}
