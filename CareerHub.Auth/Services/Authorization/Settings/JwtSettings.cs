﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace CareerHub.Auth.Services.Authorization.Settings
{
    public class JwtSettings
    {
        public const string ISSUER = "Server";
        public const string AUDIENCE = "Client";
        private static readonly string KEY = "7H5n5!v@7V^n~@Y8k7cxK%YYc3S*a9";
        public const int LIFETIME = 1;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
