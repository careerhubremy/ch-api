﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CareerHub.Auth.Services.StudentInfoParser.Models;
using CsvHelper;

namespace CareerHub.Auth.Services.StudentInfoParser
{
    public class StudentInfoParserService: IStudentInfoParserService
    {
        public IEnumerable<StudentRow> Parse(StreamReader reader)
        {
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            csv.Context.Configuration.MissingFieldFound = null;
            csv.Context.RegisterClassMap<StudentRow>();
            csv.Context.Configuration.BadDataFound = null;
            csv.Context.Configuration.Delimiter = ",";
            return csv.GetRecords<StudentRow>().ToArray();
        }
    }
}
