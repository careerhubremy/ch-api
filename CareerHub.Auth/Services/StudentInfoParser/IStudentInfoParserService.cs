﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CareerHub.Auth.Services.StudentInfoParser.Models;

namespace CareerHub.Auth.Services.StudentInfoParser
{
    public interface IStudentInfoParserService
    {
        public IEnumerable<StudentRow> Parse(StreamReader reader);
    }
}
