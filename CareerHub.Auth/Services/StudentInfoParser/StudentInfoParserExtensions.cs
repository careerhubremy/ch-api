﻿using Microsoft.Extensions.DependencyInjection;

namespace CareerHub.Auth.Services.StudentInfoParser
{
    public static class StudentInfoParserExtensions
    {
        public static void AddStudentInfoParserService(this IServiceCollection collection)
        {

            collection.AddTransient<IStudentInfoParserService, StudentInfoParserService>();
        }
    }
}
