﻿using CsvHelper;
using CsvHelper.Configuration;

namespace CareerHub.Auth.Services.StudentInfoParser.Models
{
    public sealed class StudentRow: ClassMap<StudentRow>
    {
        public string FullName { set; get; }
        public string Email { set; get; }
        public string Group { set; get; }

        public StudentRow()
        {
            Map<string>(s => s.FullName).Index(2);
            Map<string>(s => s.Email).Index(5);
            Map<string>(s => s.Group).Index(6);
        }
    }
}
