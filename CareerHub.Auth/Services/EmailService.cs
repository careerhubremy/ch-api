﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;


namespace CareerHub.Auth.Services
{
    public class EmailService
    {
        private readonly string _url = "http://careerhubmail.azurewebsites.net/";

        public async Task<(bool status, string result)> SendConfirmLinkForCompanyAsync(string to, string link)
        {
            using var client = new HttpClient();

            var data = $@"{{
              ""to"": ""{to}"",
              ""link"": ""{link}""
            }}";

            var result = await client.PostAsync(_url + "api/confirm-link", new StringContent(data, Encoding.UTF8, "application/json"));

            return (result.IsSuccessStatusCode, await result.Content.ReadAsStringAsync());
        }

        public async Task<(bool status, string result)> RestorePasswordAsync(string to, string link)
        {
            using var client = new HttpClient();

            var data = $@"{{
              ""to"": ""{to}"",
              ""link"": ""{link}""
            }}";

            var result = await client.PostAsync(_url + "api/restore-password", new StringContent(data, Encoding.UTF8, "application/json"));

            return (result.IsSuccessStatusCode, await result.Content.ReadAsStringAsync());
        }
    }
}
