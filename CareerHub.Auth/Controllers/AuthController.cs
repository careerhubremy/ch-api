﻿using CareerHub.Auth.Services;
using CareerHub.Auth.Services.Authorization;
using CareerHub.Auth.Services.Authorization.Models;
using CareerHub.DAL.Data;
using CareerHub.DAL.Models;
using CareerHub.DAL.ModelsDTO.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CareerHub.Auth.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IJwtService _jwtService;
        private readonly EmailService _emailService;
        private readonly IConfiguration _configuration;
        private readonly IHostEnvironment _hostEnvironment;

        public AuthController(DataContext context,
            IJwtService jwtService,
            EmailService emailService,
            IConfiguration configuration,
            IHostEnvironment hostEnvironment)
        {
            _configuration = configuration;
            _context = context;
            _jwtService = jwtService;
            _emailService = emailService;
            _hostEnvironment = hostEnvironment;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(AuthenticateRequest model)
        {
            var user = await _context.Users
                .Include(u => u.Role)
                .Include(x => x.Company)
                .Include(x => x.Student)
                .FirstOrDefaultAsync(x => x.Email == model.Email);

            if (user == null || user.Password != GetPasswordHash(model.Password))
            {
                return BadRequest("This combination of email and password doesn't exist");
            }

            var token = _jwtService.GetToken(new JwtUser { Login = user.UserId.ToString(), Role = user.Role.Title });

            if (user.Role.Title == "student")
            {
                return Ok(new { idToken = token, role = user.Role.Title, userId = user.UserId });
            }
            else if (user.Role.Title == "company")
            {
                var result = new { idToken = token, role = user.Role.Title, firstTime = user.Company.IsFirstTime, userId = user.UserId };

                if (user.Company.IsFirstTime)
                {
                    user.Company.IsFirstTime = false;
                    await _context.SaveChangesAsync();
                }

                return Ok(result);
            }
            else if (user.Role.Title == "admin")
            {
                var result = new { idToken = token, role = user.Role.Title, userId = user.UserId };

                return Ok(result);
            }

            return BadRequest();
        }

        [HttpPost("register/student")]
        public async Task<IActionResult> RegisterStudent(RegisterStudentRequest model)
        {
            if (await _context.Users.AnyAsync(x => x.Email == model.Email))
            {
                return BadRequest("User with such Email exists");
            }

            var studentInfoLog = await _context.StudentInfoLogs
                .SingleOrDefaultAsync(sl => sl.Email == model.Email);

            if (studentInfoLog == null)
            {
                return NotFound("Student with such email doesn't exist");
            }

            var newUser = new User
            {
                Email = studentInfoLog.Email,
                Password = GetPasswordHash(model.Password),
                Role = await _context.Roles.SingleOrDefaultAsync(x => x.Title == "student")
            };

            await _context.Users.AddAsync(newUser);

            var newStudent = new Student
            {
                User = newUser,
                FirstName = studentInfoLog.FirstName,
                LastName = studentInfoLog.LastName,
                Group = studentInfoLog.Group
            };

            _context.StudentInfoLogs.Remove(studentInfoLog);

            await _context.Students.AddAsync(newStudent);

            await _context.SaveChangesAsync();

            var token = _jwtService.GetToken(new JwtUser { Login = newUser.UserId.ToString(), Role = newUser.Role.Title });

            return Ok(new { idToken = token, role = newUser.Role.Title, userId = newUser.UserId });
        }

        [HttpPost("register/admin")]
        public async Task<IActionResult> RegisterAdmin()
        {
            if (await _context.Users.AnyAsync(x => x.Email == "admin@careerhub.com"))
            {
                return BadRequest("User with such Email exists");
            }

            var newUser = new User
            {
                Email = "admin@careerhub.com",
                Password = GetPasswordHash("k83VaBRbtzJmV6G7528dG62ud3Yn5DsH"),
                Role = await _context.Roles.SingleOrDefaultAsync(x => x.Title == "admin")
            };

            await _context.Users.AddAsync(newUser);

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPost("register/company/link")]
        public async Task<IActionResult> RegisterCompanyByLink(RegisterCompanyRequest model)
        {
            if (await _context.Users.AnyAsync(x => x.Email == model.Email))
            {
                return BadRequest("User with such Email exists");
            }
            var newUser = new User
            {
                Email = model.Email,
                Password = GetPasswordHash(model.Password),
                Role = await _context.Roles.SingleOrDefaultAsync(x => x.Title == "company")
            };

            await _context.Users.AddAsync(newUser);
            var company = new Company { User = newUser, CompanyName = model.CompanyName };
            var id = Guid.NewGuid();
            var comanyLink = new CompanyAuthLink
            {
                CompanyAuthLinkId = id,
                Company = company
            };

            var domain = _hostEnvironment.IsDevelopment() ? _configuration["FrontHost:Debug"] : _configuration["FrontHost:Production"];

            string confirmLink = domain + "/company/register/confirm/" + id;
            await _context.Companies.AddAsync(company);
            await _context.CompanyAuthLinks.AddAsync(comanyLink);
            await _context.SaveChangesAsync();

            var response = await _emailService.SendConfirmLinkForCompanyAsync(model.Email, confirmLink);
            if (response.status)
            {
                return Ok(new { id, response.result});
            }
            else
            {
                return BadRequest(response.result);
            }
        }

        [HttpGet("login/company/link/{id}")]
        public async Task<IActionResult> LoginCompanyByLink(Guid id)
        {
            var companyLink = await _context.CompanyAuthLinks
                .Include(cl => cl.Company)
                .FirstOrDefaultAsync(l => l.CompanyAuthLinkId == id);
            if (companyLink != null)
            {
                var company = await _context.Companies
                    .Include(c => c.User)
                    .ThenInclude(u => u.Role)
                    .SingleOrDefaultAsync(c => c.CompanyId == companyLink.CompanyId);
                if (company != null)
                {
                    _context.CompanyAuthLinks.Remove(companyLink);

                    await _context.SaveChangesAsync();

                    return Ok(new
                    {
                        Token = _jwtService.GetToken(new()
                        {
                            Login = company.User.UserId.ToString(),
                            Role = company.User.Role.Title
                        })
                    });
                }
            }
            return BadRequest();
        }

        [HttpPost("restore/password")]
        public async Task<IActionResult> RestorePassword(RestorePasswordRequest request)
        {
            var user = await _context.Users
               .SingleOrDefaultAsync(x => 
               x.Email == request.Email);
            if (user != null)
            {

                var id = Guid.NewGuid();

                await _context.RestorePasswordLogs.AddAsync(new()
                {
                    RestorePasswordLogId = id,
                    User = user,
                    UserId = user.UserId,
                    PasswordHash = GetPasswordHash(request.NewPassword)
                });
                await _context.SaveChangesAsync();

                var domain = _hostEnvironment.IsDevelopment() ? _configuration["FrontHost:Debug"] : _configuration["FrontHost:Production"];

                string confirmLink = domain + "/api/auth/restore/password/confirm/" + id;

                var response = await _emailService.SendConfirmLinkForCompanyAsync(user.Email, confirmLink);
                if (response.status)
                {
                    return Ok(new { id, response.result });
                }
                else
                {
                    return BadRequest(response.result);
                }
            }
            return BadRequest();
        }

        [HttpGet("restore/password/confirm/{id}")]
        public async Task<IActionResult> ConfirmNewPassword(Guid id)
        {
            var restorePasswordLog = await _context.RestorePasswordLogs
                .Include(l=>l.User)
                .SingleOrDefaultAsync(l=>l.RestorePasswordLogId == id);
            if (restorePasswordLog != null)
            {
                if (restorePasswordLog.User != null)
                {
                    restorePasswordLog.User.Password = restorePasswordLog.PasswordHash;
                    _context.RestorePasswordLogs.Remove(restorePasswordLog);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
            }
            return BadRequest();
        }

        private string GetPasswordHash(string password)
        {
            byte[] hash;
            using (var sha1 = new SHA256CryptoServiceProvider())
                hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(password));
            return Convert.ToBase64String(hash);
        }
    }
}