﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CareerHub.Auth.Services.StudentInfoParser;
using CareerHub.Auth.Services.StudentInfoParser.Models;
using CareerHub.DAL.Data;
using CareerHub.DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace CareerHub.Auth.Controllers
{
    [Route("api/auth/students")]
    [ApiController]
    public class StudentInfoController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IStudentInfoParserService _studentInfoParser;
        private readonly ILogger<StudentInfoController> _logger;
        public StudentInfoController(DataContext context, 
            IStudentInfoParserService studentInfoParser,
            ILogger<StudentInfoController> logger)
        {
            _context = context;
            _studentInfoParser = studentInfoParser;
            _logger = logger;
        }

        [HttpPost("upload")]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            if (Path.GetExtension(file.FileName) != ".csv")
            {
                return BadRequest();
            }
            using var streamReader = new StreamReader(file.OpenReadStream());
            try
            {
                IEnumerable<StudentRow> studentRows = _studentInfoParser.Parse(streamReader);
                foreach (var studentRow in studentRows)
                {
                    var studentLog = await _context.StudentInfoLogs
                        .SingleOrDefaultAsync(sl => sl.Email == studentRow.Email);
                    var user = await _context.Users.Include(u=>u.Student)
                        .SingleOrDefaultAsync(u => u.Email == studentRow.Email);
                    if (user != null)
                    {
                        user.Email = studentRow.Email;
                        user.Student.Group = studentRow.Group;
                        user.Student.FirstName = studentRow.FullName.Split(' ')[0];
                        user.Student.LastName = studentRow.FullName.Split(' ')[1];
                        await _context.SaveChangesAsync();
                    }
                    else if (studentLog != null)
                    {
                        studentLog.Email = studentRow.Email;
                        studentLog.Group = studentRow.Group;
                        studentLog.FirstName = studentRow.FullName.Split(' ')[0];
                        studentLog.LastName = studentRow.FullName.Split(' ')[1];
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        await _context.StudentInfoLogs.AddAsync(new StudentInfoLog()
                        {
                            Email = studentRow.Email,
                            LastName = studentRow.FullName.Split(' ')[0],
                            FirstName = studentRow.FullName.Split(' ')[1],
                            Group = studentRow.Group
                        });
                        await _context.SaveChangesAsync();
                    }
                }
                return Ok();
            }
            catch
            {
                _logger.LogError($"csv type error for {file.FileName}");
            }
            return BadRequest();
        }

    }
}
