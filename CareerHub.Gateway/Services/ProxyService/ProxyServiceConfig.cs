﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CareerHub.Gateway.Services.ProxyService;

namespace CareerHub.Gateway.Services.ProxyService
{
    public static class ProxyServiceConfig
    {
        public static void AddProxyService(this IServiceCollection collection)
        {
            collection.AddTransient<IProxyService, ProxyService>();
        }
    }
}
