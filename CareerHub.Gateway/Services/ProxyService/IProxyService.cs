﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CareerHub.Gateway.Services.ProxyService
{
    public interface IProxyService
    {
        public Task<HttpResponseMessage> GetProxyResponseAsync(HttpContext context, Uri targetUri);
        public Task<HttpResponseMessage> GetAuthProxyResponseAsync(HttpContext context, Uri targetUri, Guid UserId, string role);

    }
}
