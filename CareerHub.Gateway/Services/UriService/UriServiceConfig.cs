﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace CareerHub.Gateway.Services.UriService
{
    public static class UriServiceConfig
    {
        public static void AddUriService(this IServiceCollection services, IConfiguration configuration, IHostEnvironment environment)
        {
            services.AddTransient<IUriService, UriService>(p => new UriService(configuration, environment));
        }
    }
}
