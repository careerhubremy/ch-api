﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace CareerHub.Gateway.Services.UriService
{
    public class UriService : IUriService
    {
        private readonly IConfiguration _configuration;
        private readonly IHostEnvironment _env;
        public UriService(IConfiguration configuration, IHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }
        public Uri GetUri( string service, string endpoint)
        {
            var builderConfig = new ConfigurationBuilder();
            builderConfig.AddJsonFile("appsettings.json");
            var config = builderConfig.Build();
            UriBuilder builder;
            if (_env.IsDevelopment())
            {
                
                builder = new UriBuilder(
                    scheme: "http",
                    host: _configuration[$"Services:{service}"].Split('|')[0].Split(":")[0],
                    port: int.Parse(_configuration[$"Services:{service}"].Split('|')[0].Split(":")[1]),
                    path: endpoint,
                    extraValue: "");
            }
            else
            {
                builder = new UriBuilder(
                    scheme: "https",
                    host: _configuration[$"Services:{service}"].Split('|')[1].Split(":")[0],
                    port: 443,
                    path: endpoint,
                    extraValue: "");
            }

           
            var uri = builder.Uri;
            Console.WriteLine(uri.ToString());
            return uri;
        }
    }
}
