﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace CareerHub.Gateway.Services.UriService
{
    public interface IUriService
    {
        public Uri GetUri(string service, string endpoint);
    }
}
