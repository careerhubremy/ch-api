﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CareerHub.DAL.Data;
using CareerHub.DAL.Models;
using CareerHub.Gateway.Services.UriService;
using CareerHub.Gateway.Services.ProxyService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.Security.Claims;

namespace CareerHub.Gateway.Controllers
{
    [Route("api/jobOffers")]
    [ApiController]
    public class JobOffersController : ControllerBase
    {
        private readonly IProxyService _proxyService;
        private readonly IUriService _uriService;

        public JobOffersController(IProxyService proxyService,
            IUriService uriService)
        {
            _uriService = uriService;
            _proxyService = proxyService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetJobOffers()
        {
            var targetUri = _uriService.GetUri( "DataService", "api/jobOffers");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("chunk/{chunk}")]
        [Authorize]
        public async Task<IActionResult> GetJobOffers(int chunk)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/jobOffers/chunk/{chunk}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("jobOffers/ByCompany/{companyId}")]
        [Authorize]
        public async Task<IActionResult> GetJobOffers(Guid companyId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/jobOffers/ByCompany/{companyId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("jobOffers/ByCompany/{companyId}/{chunk}")]
        [Authorize]
        public async Task<IActionResult> GetJobOffers(Guid companyId, int chunk)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/jobOffers/ByCompany/{companyId}/{chunk}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<JobOffer>> GetJobOffer(Guid id)
        {
            var targetUri = _uriService.GetUri( "DataService", $"api/jobOffers/{id}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);

            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpPost]
        [Authorize(Roles = "company")]
        public async Task<ActionResult<JobOffer>> PostJobOffer(JobOffer jobOffer)
        {
            var targetUri = _uriService.GetUri( "DataService", $"api/jobOffers");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);

            if (response.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(await response.Content.ReadAsStringAsync());
        }

        [HttpPut]
        [Authorize(Roles = "company")]
        public async Task<IActionResult> PutJobOffer(JobOffer model)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/jobOffers");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);

            if (response.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(await response.Content.ReadAsStringAsync());
        }
    }
}
