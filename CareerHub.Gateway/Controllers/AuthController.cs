﻿using CareerHub.DAL.ModelsDTO.Requests;
using CareerHub.Gateway.Services.UriService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using CareerHub.Gateway.Services.ProxyService;
using Microsoft.Extensions.Hosting;
using CareerHub.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using CareerHub.DAL.Data;

namespace CareerHub.Gateway.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IProxyService _proxyService;
        private readonly IUriService _uriService;
        private readonly IConfiguration _configuration;
        private readonly IHostEnvironment _hostEnvironment;

        public AuthController(IProxyService proxyService,
            IUriService uriService,
            DataContext dataContext,
            IConfiguration configuration,
            IHostEnvironment hostEnvironment)
        {
            _proxyService = proxyService;
            _uriService = uriService;
            _configuration = configuration;
            _hostEnvironment = hostEnvironment;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(AuthenticateRequest request)
        {
            var targetUri = _uriService.GetUri("AuthService", "api/auth/login");
            var response = await _proxyService.GetProxyResponseAsync(HttpContext, targetUri);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest(await response.Content.ReadAsStringAsync());
        }

        [HttpPost("register/student")]
        public async Task<IActionResult> RegisterStudent(RegisterStudentRequest request)
        {
            var targetUri = _uriService.GetUri( "AuthService", "api/auth/register/student");
            var response = await _proxyService.GetProxyResponseAsync(HttpContext, targetUri);

            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest(await response.Content.ReadAsStringAsync());
        }

        [HttpPost("restore/password")]
        public async Task<IActionResult> RestorePassword(RestorePasswordRequest request)
        {
            var targetUri = _uriService.GetUri("AuthService", $"api/auth/restore/password");
            var response = await _proxyService.GetProxyResponseAsync(HttpContext, targetUri);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest(await response.Content.ReadAsStringAsync());
        }

        [HttpPost("register/admin")]
        public async Task<IActionResult> RegisterAdmin()
        {
            var targetUri = _uriService.GetUri("AuthService", $"api/auth/register/admin");
            var response = await _proxyService.GetProxyResponseAsync(HttpContext, targetUri);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest(await response.Content.ReadAsStringAsync());
        }

        [HttpPost("register/company/link")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> RegisterCompanyByLink(RegisterCompanyRequest model)
        {
            var targetUri = _uriService.GetUri("AuthService", $"api/auth/register/company/link");
            var response = await _proxyService.GetProxyResponseAsync(HttpContext, targetUri);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest(await response.Content.ReadAsStringAsync());
        }

        [HttpGet("login/company/link/{id}")]
        public async Task<IActionResult> LoginCompanyByLink(Guid id)
        {
            var targetUri = _uriService.GetUri("AuthService", $"api/auth/login/company/link/{id}");
            var response = await _proxyService.GetProxyResponseAsync(HttpContext, targetUri);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest(await response.Content.ReadAsStringAsync());
        }


        [HttpGet("restore/password/confirm/{id}")]
        public async Task<IActionResult> ConfirmNewPassword(Guid id)
        {
            var targetUri = _uriService.GetUri("AuthService", $"api/auth/restore/password/confirm/{id}");
            var response = await _proxyService.GetProxyResponseAsync(HttpContext, targetUri);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest(await response.Content.ReadAsStringAsync());
        }
    }
}