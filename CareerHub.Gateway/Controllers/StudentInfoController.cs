﻿using System.Threading.Tasks;
using CareerHub.DAL.Models;
using CareerHub.Gateway.Services.ProxyService;
using CareerHub.Gateway.Services.UriService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CareerHub.Gateway.Controllers
{
    [Route("api/auth/students")]
    [ApiController]
    public class StudentInfoController : ControllerBase
    {
        private readonly IProxyService _proxyService;
        private readonly IUriService _uriService;

        public StudentInfoController(IProxyService proxyService, IUriService uriService)
        {
            _proxyService = proxyService;
            _uriService = uriService;
        }

        [HttpPost("upload")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            var targetUri = _uriService.GetUri("AuthService", "api/auth/students/upload");
            var response = await _proxyService.GetProxyResponseAsync(HttpContext, targetUri);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }
    }
}
