﻿using CareerHub.DAL.Models;
using CareerHub.DAL.ModelsDTO.Requests;
using CareerHub.Gateway.Services.ProxyService;
using CareerHub.Gateway.Services.UriService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CareerHub.Gateway.Controllers
{
    [Route("api/students")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IProxyService _proxyService;
        private readonly IUriService _uriService;

        public StudentsController(IProxyService proxyService,
            IUriService uriService)
        {
            _uriService = uriService;
            _proxyService = proxyService;
        }

        [HttpGet("all")]
        [Authorize]
        public async Task<IActionResult> GetStudents()
        {
            var targetUri = _uriService.GetUri("DataService", "api/students/all");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("all/chunk/{chunk}")]
        [Authorize]
        public async Task<IActionResult> GetStudents(int chunk)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/students/all/chunk/{chunk}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("one/{userId}")]
        [Authorize]
        public async Task<IActionResult> GetStudent(Guid userId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/students/one/{userId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpPost("create")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> PostStudent(CreateStudentRequest student)
        {
            var targetUri = _uriService.GetUri("DataService", "api/students/create");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        // admin student
        [HttpDelete("delete/{userId?}")]
        [Authorize(Roles = "admin,student")]
        public async Task<IActionResult> DeleteStudent(Guid userId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/students/delete/{userId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        // admin student
        [HttpPut("edit")]
        [Authorize(Roles = "admin,student")]
        public async Task<IActionResult> UpdateStudent(Student model)
        {
            var targetUri = _uriService.GetUri("DataService", "api/students/edit");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }
    }
}
