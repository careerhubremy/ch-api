﻿using CareerHub.DAL.Models;
using CareerHub.DAL.ModelsDTO.Requests;
using CareerHub.Gateway.Services.ProxyService;
using CareerHub.Gateway.Services.UriService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;             
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CareerHub.Gateway.Controllers
{
    [Route("api/experiences")]
    [ApiController]
    public class ExperiencesController : ControllerBase
    {
        private readonly IProxyService _proxyService;
        private readonly IUriService _uriService;

        public ExperiencesController(IProxyService proxyService,
            IUriService uriService)
        {
            _uriService = uriService;
            _proxyService = proxyService;
        }

        [HttpGet("all")]
        [Authorize(Roles = "student")]
        public async Task<IActionResult> GetExperiences()
        {
            var targetUri = _uriService.GetUri("DataService", "api/experiences/all");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("allOfStudent")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> GetExperiencesOfStudent(Guid userId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/experiences/allOfStudent/{userId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpPost("create")]
        [Authorize(Roles = "student")]
        public async Task<IActionResult> PostStudent(Experience experience)
        {
            var targetUri = _uriService.GetUri("DataService", "api/experiences/create");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpDelete("delete/{experienceId}")]
        [Authorize(Roles = "student")]
        public async Task<IActionResult> DeleteExperience(Guid experienceId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/experiences/delete/{experienceId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpPut("edit")]
        [Authorize(Roles = "student")]
        public async Task<IActionResult> UpdateStudent(Experience experience)
        {
            var targetUri = _uriService.GetUri("DataService", "api/experiences/edit");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }
    }
}
