﻿using CareerHub.DAL.Models;
using CareerHub.Gateway.Services.ProxyService;
using CareerHub.Gateway.Services.UriService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CareerHub.Gateway.Controllers
{
    [Route("api/subscriptions")]
    [ApiController]
    public class SubscriptionsController : ControllerBase
    {
        private readonly IProxyService _proxyService;
        private readonly IUriService _uriService;

        public SubscriptionsController(IProxyService proxyService,
            IUriService uriService)
        {
            _uriService = uriService;
            _proxyService = proxyService;
        }

        [HttpGet("companies")]
        [Authorize(Roles = "student")]
        public async Task<ActionResult<IEnumerable<Company>>> GetSubscriptionsCompany()
        {
            var targetUri = _uriService.GetUri("DataService", "api/subscriptions/companies");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("companies/{chunk}")]
        [Authorize(Roles = "student")]
        public async Task<ActionResult<IEnumerable<Company>>> GetSubscriptionsCompany(int chunk)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/companies/{chunk}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("companiesOfStudent/{studentId}")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<Company>>> GetSubscriptionsCompanyOfStudent(Guid studentId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/companiesOfStudent/{studentId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("companiesOfStudent/{studentId}/{chunk}")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<Company>>> GetSubscriptionsCompanyOfStudent(Guid studentId, int chunk)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/companiesOfStudent/{studentId}/{chunk}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("jobOffers")]
        [Authorize(Roles = "student")]
        public async Task<ActionResult<IEnumerable<JobOffer>>> GetSubscriptionsJobOffer()
        {
            var targetUri = _uriService.GetUri("DataService", "api/subscriptions/jobOffers");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("jobOffers/{chunk}")]
        [Authorize(Roles = "student")]
        public async Task<ActionResult<IEnumerable<JobOffer>>> GetSubscriptionsJobOffer(int chunk)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/jobOffers/{chunk}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("jobOffersOfStudent/{studentId}")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<JobOffer>>> GetSubscriptionsJobOfferOfStudent(Guid studentId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/jobOffersOfStudent/{studentId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("jobOffersOfStudent/{userId}/{chunk}")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<JobOffer>>> GetSubscriptionsJobOfferOfStudent(Guid userId, int chunk)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/jobOffersOfStudent/{userId}/{chunk}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpPut("subscribe/company/{companyId}")]
        [Authorize(Roles = "student")]
        public async Task<IActionResult> PutSubscribeToCompany(Guid companyId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/subscribe/company/{companyId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpPut("unsubscribe/company/{companyId}")]
        [Authorize(Roles = "student")]
        public async Task<IActionResult> UnsubscribeFromCompany(Guid companyId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/unsubscribe/company/{companyId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpPut("subscribe/jobOffer/{id}")]
        public async Task<IActionResult> SubscribeToJobOffer(Guid id)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/subscribe/jobOffer/{id}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpPut("unsubscribe/jobOffer/{id}")]
        public async Task<IActionResult> UnsubscribeToJobOffer(Guid id)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/unsubscribe/jobOffer/{id}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("students")]
        [Authorize(Roles = "student")]
        public async Task<ActionResult<IEnumerable<JobOffer>>> GetSubscriptionsStudents()
        {
            var targetUri = _uriService.GetUri("DataService", "api/subscriptions/students");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            return StatusCode((int)response.StatusCode, await response.Content.ReadAsStringAsync());
        }

        [HttpPut("unsubscribe/student/{studentId}")]
        public async Task<IActionResult> UnsubscribeFromStudent(Guid studentId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/unsubscribe/student/{studentId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            return StatusCode((int)response.StatusCode, await response.Content.ReadAsStringAsync());
        }

        [HttpPut("subscribe/student/{studentId}")]
        public async Task<IActionResult> SubscribeToStudent(Guid studentId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/subscriptions/subscribe/student/{studentId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            return StatusCode((int)response.StatusCode, await response.Content.ReadAsStringAsync());
        }
    }
}
