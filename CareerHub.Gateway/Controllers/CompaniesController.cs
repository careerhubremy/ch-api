﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CareerHub.DAL.Data;
using CareerHub.DAL.Models;
using CareerHub.Gateway.Services.ProxyService;
using CareerHub.Gateway.Services.UriService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.Security.Claims;

namespace CareerHub.Gateway.Controllers
{
    [Route("api/companies")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly IProxyService _proxyService;
        private readonly IUriService _uriService;


        public CompaniesController(IProxyService proxyService,
            IUriService uriService)
        {
            _uriService = uriService;
            _proxyService = proxyService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetCompanies()
        {
            var targetUri = _uriService.GetUri("DataService", "api/companies");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("chunk/{chunk}")]
        [Authorize]
        public async Task<IActionResult> GetCompanies(int chunk)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/companies/chunk/{chunk}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("{companyId}")]
        [Authorize]
        public async Task<ActionResult<Company>> GetCompany(Guid companyId)
        {
            var targetUri = _uriService.GetUri( "DataService", $"api/companies/{companyId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);

            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpDelete("delete/{companyId}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteCompany(Guid companyId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/companies/delete/{companyId}");
            var response = await _proxyService.GetProxyResponseAsync(HttpContext, targetUri);

            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpPut("edit/{companyId}")]
        [Authorize(Roles = "admin,company")]
        public async Task<IActionResult> UpdateCompany(Guid companyId, Company model)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/companies/edit/{companyId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }
    }
}