﻿using CareerHub.DAL.Models;
using CareerHub.DAL.ModelsDTO.Requests;
using CareerHub.Gateway.Services.ProxyService;
using CareerHub.Gateway.Services.UriService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CareerHub.Gateway.Controllers
{
    [Route("api/cv")]
    [ApiController]
    public class CVsController : ControllerBase
    {
        private readonly IProxyService _proxyService;
        private readonly IUriService _uriService;

        public CVsController(IProxyService proxyService,
            IUriService uriService)
        {
            _uriService = uriService;
            _proxyService = proxyService;
        }

        [HttpGet("all")]
        [Authorize(Roles = "student")]
        public async Task<IActionResult> GetCVs()
        {
            var targetUri = _uriService.GetUri("DataService", "api/cv/all");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpGet("allOfStudent")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> GetCVsOfStudent(Guid userId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/cv/allOfStudent/{userId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpPost("create")]
        [Authorize(Roles = "student")]
        public async Task<IActionResult> PostCV(CV cv)
        {
            var targetUri = _uriService.GetUri("DataService", "api/cv/create");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }

        [HttpDelete("delete/{CVId}")]
        [Authorize(Roles = "student")]
        public async Task<IActionResult> DeleteCV(Guid CVId)
        {
            var targetUri = _uriService.GetUri("DataService", $"api/cv/delete/{CVId}");
            var response = await _proxyService.GetAuthProxyResponseAsync(
                HttpContext,
                targetUri,
                Guid.Parse(HttpContext.User.Identity.Name),
                HttpContext.User.FindFirst(ClaimTypes.Role).Value);
            if (response.IsSuccessStatusCode)
            {
                return Ok(await response.Content.ReadAsStringAsync());
            }
            return BadRequest();
        }
    }
}
